﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using LibGit2Sharp;
using Microsoft.Build.Evaluation;
using Microsoft.Build.Execution;
using Microsoft.Build.Framework;
using Microsoft.Build.Logging;
using NuGet;


namespace BuildConsoleApp
{
    class Program
    {
        private static string Output { get; set; }
        private static string Authors { get; set; }
        private static string ApiKey { get; set; }
        private static string RepoUrl { get; set; }
        private static string Hash { get; set; }
        private static string RepoDir { get; set; }
        private static string PackageSource { get; set; }
        private static int Timeout { get; set; }

        private static BackgroundWorker _worker = new BackgroundWorker();
        

        static void Main(string[] args)
        {

            ApiKey = ConfigurationManager.AppSettings.Get("ApiKey");
            RepoUrl = ConfigurationManager.AppSettings.Get("RepoUrl");
            PackageSource = ConfigurationManager.AppSettings.Get("PackageSource");
            RepoDir = RepoUrl.Split('/').Last().Split('.').First();
            Timeout = Convert.ToInt32(ConfigurationManager.AppSettings.Get("Timeout"));
            _worker.DoWork += Worker_DoWork;
            _worker.WorkerSupportsCancellation = true;
            Console.WriteLine(DateTime.Now + ": Starting Application...");
            while (true)
            {
                _worker.RunWorkerAsync();
                Thread.Sleep(Timeout * 1000);
            }
        }

        private static void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (Directory.Exists(Directory.GetCurrentDirectory() + "\\" + RepoDir + "\\") == false)
            {
                RepoClone(RepoUrl);
                PullDiff();
                BuildSolution();
                
            }
            else
            {
                RepoFetch();
                CheckDiff();
            }
                       
        }

        static void RepoClone(string url)
        {
            Console.WriteLine(DateTime.Now + ": Clone the repository from " + RepoUrl);
            Repository.Clone(url, Directory.GetCurrentDirectory() + "/" + RepoDir);
        }

        static void RepoFetch()
        {
            string logMessage = "";
            using (var repo = new Repository(Directory.GetCurrentDirectory() + "/" + RepoDir))
            {
                foreach (Remote remote in repo.Network.Remotes)
                {
                    IEnumerable<string> refSpecs = remote.FetchRefSpecs.Select(x => x.Specification);
                    Commands.Fetch(repo, remote.Name, refSpecs, null, logMessage);
                }
            }
            Console.WriteLine(logMessage);
        }

        static void CheckDiff()
        {
            Console.WriteLine(DateTime.Now + ": Сheck for changes to the repository " + RepoUrl);
            using (var repo = new Repository(Directory.GetCurrentDirectory() + "/" + RepoDir))
            {
                var trackingBranch = repo.Head.TrackedBranch;
                var log = repo.Commits.QueryBy(new CommitFilter
                { IncludeReachableFrom = trackingBranch.Tip.Id, ExcludeReachableFrom = repo.Head.Tip.Id });

                var count = log.Count();
                if (count > 0)
                {
                    PullDiff();
                    BuildSolution();                    
                }
                else if(ConfigurationManager.AppSettings.Get("BuildSolutionStatus") == "false" && count == 0)
                {
                    BuildSolution();
                }
                else if (ConfigurationManager.AppSettings.Get("PushPackageStatus") == "false" && count == 0)
                {
                    BuildPackage();
                }
                else
                {
                    Console.WriteLine(DateTime.Now + ": Nothing to do");
                }
            }
        }

        private static void PushPackage()
        {
            //пока не работает
            //var localRepo = PackageRepositoryFactory.Default.CreateRepository(Directory.GetCurrentDirectory() + "\\nugetrepo\\");
            //var package = localRepo.FindPackagesById("MyKonturTestPackage").First();
            //var packageFile = new FileInfo(Directory.GetCurrentDirectory() + "\\nugetrepo\\" + "MyKonturTestPackage." + FileVersionInfo
            //                                   .GetVersionInfo(Directory.GetFiles(Directory.GetCurrentDirectory() + "\\" + RepoDir + "\\bin\\Release",
            //                                       "*.exe")[0]).FileVersion + ".nupkg");
            //var size = packageFile.Length;
            //var ps = new PackageServer("https://nuget.org/", "userAgent");
            //ps.PushPackage("oy2lv3uletjbwyfdw4maccfasgmsotrercoevjkwdp7hg4", package, size, 5000, false);

            //це костiль
            Process pushProcess = new Process();
            Console.WriteLine(DateTime.Now + ": Pushing package " + RepoDir + "MyKonturTestPackage." +
                              FileVersionInfo.GetVersionInfo(Directory.GetFiles(Directory.GetCurrentDirectory() +
                                                                                "\\" + RepoDir + "\\bin\\Release", "*.exe")[0]).FileVersion + ".nupkg");
            pushProcess.StartInfo.FileName = Directory.GetCurrentDirectory() + "\\nuget\\nuget.exe";
            pushProcess.StartInfo.UseShellExecute = false;
            pushProcess.StartInfo.RedirectStandardOutput = true;
            pushProcess.StartInfo.RedirectStandardOutput = true;
            pushProcess.StartInfo.Arguments = "push " + Directory.GetCurrentDirectory() + "\\nugetrepo\\" + RepoDir + "MyKonturTestPackage." +
                                              FileVersionInfo.GetVersionInfo(Directory.GetFiles(Directory.GetCurrentDirectory() +
                                                                                                "\\" + RepoDir + "\\bin\\Release", "*.exe")[0]).FileVersion + ".nupkg" + " -ApiKey " +
                                              ApiKey + " -Source " + PackageSource;
            pushProcess.Start();
            string result = pushProcess.StandardOutput.ReadToEnd();
            pushProcess.WaitForExit();
            if (result.Contains("Your package was pushed") == false)
            {
                Console.WriteLine(DateTime.Now + ": Package has not pushed" + "\n" + result);
                ConfigurationManager.AppSettings.Set("PushPackageStatus","false");
            }
            else
            {
                Console.WriteLine(result);
                ConfigurationManager.AppSettings.Set("PushPackageStatus", "true");
            }
        }

        private static void BuildPackage()
        {
            Console.WriteLine(DateTime.Now + ": Building package " + RepoDir + "MyKonturTestPackage." +
                              FileVersionInfo.GetVersionInfo(Directory.GetFiles(Directory.GetCurrentDirectory() +
                                                                                "\\" + RepoDir + "\\bin\\Release", "*.exe")[0]).FileVersion + ".nupkg");
            ManifestMetadata metadata = new ManifestMetadata()
            {
                Authors = Authors,
                Version = FileVersionInfo
                       .GetVersionInfo(Directory.GetFiles(Directory.GetCurrentDirectory() + "\\" + RepoDir + "\\bin\\Release",
                           "*.exe")[0]).FileVersion,
                Id = RepoDir + "MyKonturTestPackage",
                Description = Output + '\n' + Hash
            };
            PackageBuilder builder = new PackageBuilder();
            builder.PopulateFiles(Directory.GetCurrentDirectory() + "\\" + RepoDir + "", new[] { new ManifestFile { Source = "**", Target = "content" } });
            builder.Populate(metadata);
            System.IO.Directory.CreateDirectory(Directory.GetCurrentDirectory() + "\\nugetrepo\\");
            using (FileStream stream = File.Open(Directory.GetCurrentDirectory() + "\\nugetrepo\\" + RepoDir + "MyKonturTestPackage." + FileVersionInfo
                                                     .GetVersionInfo(Directory.GetFiles(Directory.GetCurrentDirectory() + "\\" + RepoDir + "\\bin\\Release",
                                                         "*.exe")[0]).FileVersion + ".nupkg", FileMode.OpenOrCreate))
            {
                builder.Save(stream);
                
            }
            PushPackage();
        }

        private static void RunApplication()
        {
            string appName = Directory.GetFiles(Directory.GetCurrentDirectory() + "\\" + RepoDir + "\\", "*.sln")[0].Split('\\')
                .Last().Split('.').First();
            Console.WriteLine(DateTime.Now + ": Running Application " + Directory.GetCurrentDirectory() + "\\" + RepoDir + "\\bin\\Release\\" + appName + ".exe");
            if (File.Exists(Directory.GetCurrentDirectory() + "\\" + RepoDir + "\\bin\\Release\\" + appName + ".exe"))
            {
                Process app = new Process();

                app.StartInfo.FileName = Directory.GetCurrentDirectory() + "\\" + RepoDir + "\\bin\\Release\\" + appName + ".exe";
                app.StartInfo.UseShellExecute = false;
                app.StartInfo.RedirectStandardOutput = true;
                app.Start();
                Output = app.StandardOutput.ReadToEnd();
                Console.WriteLine("Output: \n");
                Console.WriteLine(Output);
                app.WaitForExit();
                File.WriteAllText(Directory.GetCurrentDirectory() + "\\output.txt", Output, Encoding.Unicode);
                BuildPackage();
                
            }
            else
            {
                Console.WriteLine(DateTime.Now + ": No app .exe file in folder");
            }
        }

        private static void BuildSolution()
        {
            string solutionFileName = Directory.GetFiles(Directory.GetCurrentDirectory() + "\\" + RepoDir + "\\", "*.sln")[0];
            Console.WriteLine(DateTime.Now + ": Building Solution " + solutionFileName);
            using (ProjectCollection pc = new ProjectCollection())
            {
                Dictionary<string, string> globalProperty = new Dictionary<string, string>
            {
                { "Configuration", "Release" },
                { "Platform", "Any CPU" },
                { "OutputPath", Directory.GetCurrentDirectory() + "\\" + RepoDir + "\\bin\\Release" }
            };

                BuildParameters bp = new BuildParameters(pc);
                bp.Loggers = new[] {
                new FileLogger
                {
                    Verbosity = LoggerVerbosity.Detailed,
                    ShowSummary = true,
                    SkipProjectStartedText = true
                }
            };

                BuildManager.DefaultBuildManager.BeginBuild(bp);
                BuildRequestData buildRequest = new BuildRequestData(solutionFileName, globalProperty, null, new string[] { "Build" }, null);
                BuildSubmission buildSubmission = BuildManager.DefaultBuildManager.PendBuildRequest(buildRequest);
                buildSubmission.Execute();
                BuildManager.DefaultBuildManager.EndBuild();
                if (buildSubmission.BuildResult.OverallResult == BuildResultCode.Failure)
                {
                    Console.WriteLine(DateTime.Now + ": Build failed!\n" + "See the " + Directory.GetCurrentDirectory() + "\\msbuild file for details.");
                    ConfigurationManager.AppSettings.Set("BuildSolutionStatus", "false");
                }
                else
                {
                    ConfigurationManager.AppSettings.Set("BuildSolutionStatus", "true");
                    RunApplication();
                }
            }
        }

        private static void PullDiff()
        {
            Console.WriteLine(DateTime.Now + ": pulling difference");
            using (var repo = new Repository(Directory.GetCurrentDirectory() + "/" + RepoDir))
            {
                PullOptions options = new PullOptions
                {
                    FetchOptions = new FetchOptions()
                };
                var signature = new Signature(
                    new Identity("MERGE_USER_NAME", "MERGE_USER_EMAIL"), DateTimeOffset.Now);
                Commands.Pull(repo, signature, options);
                Authors = repo.Head.Tip.Author.Name;
                Hash = repo.Head.Tip.Sha;
            }
        }
    }

}

